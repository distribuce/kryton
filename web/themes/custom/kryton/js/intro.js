jQuery(document).ready(function($){

    $('.view-intro .view-content').addClass('owl-carousel owl-theme');

    $('.view-intro .owl-carousel').owlCarousel({
        items: 1,
        loop:true,
        margin:0,
        autoplay:true,
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        responsive:{
            0:{
                nav:false,
                autoplay:false,
            },
            768:{
                nav:false,
                autoplay:true,
            },
            1460:{
                nav:true,
                autoplay:true,
            }
        }
    }) 

});
